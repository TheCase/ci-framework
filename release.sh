set -ex

USERNAME=registry.311cub.net:5050
IMAGE=ci-framework

# get latest
git pull
# bump version
#docker run --rm -v "$PWD":/app TheCase/bump patch
version=`cat VERSION`
echo "version: $version"

# run build
./build.sh
# tag it
git add -A
git commit -m "version $version"
git tag -a "$version" -m "version $version"
git push
git push --tags

# tag it 
#docker tag $USERNAME/$IMAGE:latest $USERNAME/$IMAGE:$version

# bag it
#docker push $USERNAME/$IMAGE:latest
#docker push $USERNAME/$IMAGE:$version
