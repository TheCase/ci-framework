set -ex

USERNAME=registry.311cub.net:5050
IMAGE=ci-framework

docker build -t $USERNAME/$IMAGE:latest .
