concourse.ci instructions

Install fly (link on concourse main page)

login:
```
fly -t ci login -c http://concourse.service.consul:8080
```

push up pipeline
```
fly -t ci set-pipeline -p ci-framework -c ci/pipeline.yml 
```
